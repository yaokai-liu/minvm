cmake_minimum_required(VERSION 3.28)
project(minVM CXX ASM)

set(CMAKE_CXX_STANDARD 20)

include_directories(instruction)
include_directories(execution)

add_executable(minVM main.cpp)
