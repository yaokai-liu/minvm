/**
 * Project Name: minVM
 * Module Name: instruction
 * Filename: Instruction.h
 * Creator: Yaokai Liu
 * Create Date: 24-5-6
 * Copyright (c) 2024 Yaokai Liu. All rights reserved.
 **/

#ifndef MINVM_INSTRUCTION_H
#define MINVM_INSTRUCTION_H

#include <cstdint>
#include <string>

struct Instruction {
    Instruction() = default;
    virtual std::string toString() = 0;
protected:
    uint16_t opcode{};
};


#endif //MINVM_INSTRUCTION_H
