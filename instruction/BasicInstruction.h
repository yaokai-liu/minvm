/**
 * Project Name: minVM
 * Module Name: instruction
 * Filename: BasicInstruction.h
 * Creator: Yaokai Liu
 * Create Date: 24-5-6
 * Copyright (c) 2024 Yaokai Liu. All rights reserved.
 **/

#ifndef MINVM_BASIC_INSTRUCTION_H
#define MINVM_BASIC_INSTRUCTION_H

#include "Instruction.h"
#include <format>

struct BasicInstruction : Instruction {
    typedef enum : uint16_t {
        nop,
        load,
        store,
        smv, // shift move
        jump,
    } __BASIC_OPCODE_ENUM__; // NOLINT(*-reserved-identifier)
};

struct NopInstruction: BasicInstruction {
    NopInstruction() { opcode = nop; }
    std::string toString() override {
        return "nop";
    }
};

struct LoadInstruction: BasicInstruction {
    uint16_t        rd{};
    uint16_t        rs{};
    int16_t        offset{};
    LoadInstruction() { opcode = load; }
    std::string toString() override {
        return std::format("load {}[{}] -> {}", rs, offset, rd);
    }
};

struct StoreInstruction: BasicInstruction {
    uint16_t        rd{};
    uint16_t        rs{};
    int16_t        offset{};
    StoreInstruction() { opcode = store; }
    std::string toString() override {
        return std::format("store {} -> {}[{}]", rs, rd, offset);
    }
};

struct ShiftMoveInstruction: BasicInstruction {
    uint16_t        rd{};
    uint16_t        rs{};
    int16_t         count{};
    ShiftMoveInstruction() { opcode = smv; }
    std::string toString() override {
        return std::format("smv {}<<{} -> {}", rs, count, rd);
    }
};


struct JumpInstruction: BasicInstruction {
    uint64_t    address{};
    JumpInstruction() { opcode = jump; }
    std::string toString() override {
        return std::format("jump {}", address);
    }
};


#endif //MINVM_BASIC_INSTRUCTION_H
