/**
 * Project Name: minVM
 * Module Name: instruction
 * Filename: ExtendJumpInstruction.h
 * Creator: Yaokai Liu
 * Create Date: 24-5-6
 * Copyright (c) 2024 Yaokai Liu. All rights reserved.
 **/

#ifndef MINVM_EXTEND_JUMP_INSTRUCTION_H
#define MINVM_EXTEND_JUMP_INSTRUCTION_H

#include "Instruction.h"

struct ExtendJumpInstruction : Instruction {
    typedef enum : uint16_t {
        jump_if_eq,
        jump_if_ne,
        jump_if_gt,
        jump_if_lt,
        call,
        ret,
    } __JUMP_OPCODE_ENUM__; // NOLINT(*-reserved-identifier)
};


#endif //MINVM_EXTEND_JUMP_INSTRUCTION_H
