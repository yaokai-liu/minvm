/**
 * Project Name: minVM
 * Module Name: instruction
 * Filename: ArithInstruction.h
 * Creator: Yaokai Liu
 * Create Date: 24-5-6
 * Copyright (c) 2024 Yaokai Liu. All rights reserved.
 **/

#ifndef MINVM_ARITH_INSTRUCTION_H
#define MINVM_ARITH_INSTRUCTION_H

#include "Instruction.h"

struct ArithInstruction : Instruction {
    typedef enum : uint16_t {

    } __ARITH_OPCODE_ENUM__; // NOLINT(*-reserved-identifier)
};


#endif //MINVM_ARITH_INSTRUCTION_H
