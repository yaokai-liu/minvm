/**
 * Project Name: minVM
 * Module Name: execution
 * Filename: Execution.h
 * Creator: Yaokai Liu
 * Create Date: 24-5-6
 * Copyright (c) 2024 Yaokai Liu. All rights reserved.
 **/

#ifndef MINVM_EXECUTION_H
#define MINVM_EXECUTION_H

#include <cstdint>

struct Execution {
    Execution() = default;
private:
    void *      entry;
    size_t      size;
};


#endif //MINVM_EXECUTION_H
